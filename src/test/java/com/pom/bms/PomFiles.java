package com.pom.bms;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import io.github.bonigarcia.wdm.WebDriverManager;

public class PomFiles {
	
	  public PomFiles(WebDriver driver) {
		  PageFactory.initElements(driver, this);
		
	}
	  static String noOfTickets = "2";
	public static WebElement location(WebDriver driver) {

	        return driver.findElement(By.xpath("//span[@class='sc-kaNhvL jlISnX ellipsis']"));
	    }

	    public static WebElement searchForCity(WebDriver driver) {

	        return driver.findElement(By.xpath("//input[@placeholder='Search for your city']"));
	    }
	    public static WebElement movieSearch(WebDriver driver) {
	        return  driver.findElement(By.xpath("//span[text()='Search for Movies, Events, Plays, Sports and Activities']"));
	    }
	    public static WebElement movienameEnter(WebDriver driver) {
	        return  driver.findElement(By.xpath("//*[@id=\"super-container\"]/div[2]/div[2]/div[2]/div[1]/div/div[2]/div/div/div/input"));
	    }
	    public static WebElement bookTicketsbtn(WebDriver driver) {
	        return  driver.findElement(By.xpath("//*[@id=\"super-container\"]/div[2]/section[1]/div/div/div[2]/div[2]/div/button"));
	    }

	    

	    public static WebElement dateClick(WebDriver driver) {

	        return driver.findElement(By.xpath("//a[contains(@href,'tomorrow')][1]"));
	    }

	    public static WebElement showTime(WebDriver driver) {

	        return driver.findElement(By.xpath("//a[@data-date-time='06:30 PM']"));
	    }

	    public static WebElement acceptpopupButton(WebDriver driver) {

	        return driver.findElement(By.id("btnPopupAccept"));
	    }
	    public static WebElement tickets(WebDriver driver) {


	        return driver.findElement(By.xpath(".//*[@id='popQty']/*[text()='"+noOfTickets+"']"));
	    }

	    public static WebElement procceedQty(WebDriver driver) {

	        return driver.findElement(By.xpath("//*[@id=\"proceed-Qty\"]"));
	    }


	    public static WebElement btmcmntBook(WebDriver driver) {


	        return driver.findElement(By.id("btmcntbook"));
	    }

	    public static WebElement shmTicket(WebDriver driver) {

	        return driver.findElement(By.id("shmticket"));
	    }
	    public static WebElement prepay(WebDriver driver) {


	        return driver.findElement(By.id("prePay"));
	    }
	   

	    

	    
}
