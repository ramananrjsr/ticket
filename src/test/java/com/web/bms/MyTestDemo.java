package com.web.bms;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import java.awt.AWTException;
import java.awt.GraphicsConfiguration;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.monte.screenrecorder.ScreenRecorder;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pom.bms.PomFiles;

import io.github.bonigarcia.wdm.WebDriverManager;
public class MyTestDemo {

		 

		WebDriver driver;
		 static String noOfTickets = "2";
		 static String Url= "https://in.bookmyshow.com/explore/home/chennai";
	     PomFiles pom = new PomFiles(driver);
	@BeforeClass
	public void browser() {
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		driver=new ChromeDriver(options);
		driver.get(Url);
		driver.manage().window().maximize();
		}
		
	@Test(dataProvider="Login",groups="Booking")
	public void fb(String movie,String location) throws InterruptedException, AWTException {
		PomFiles.location(driver).click();
	    Thread.sleep(2000);
		PomFiles.searchForCity(driver).sendKeys(location);
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		PomFiles.movieSearch(driver).click();
		PomFiles.movienameEnter(driver).sendKeys(movie);
		Thread.sleep(2000);
	    Robot robot1 = new Robot();
	    robot1.keyPress(KeyEvent.VK_ENTER);
	    robot1.keyRelease(KeyEvent.VK_ENTER);
	    PomFiles.bookTicketsbtn(driver).click();
	    PomFiles.dateClick(driver).click();
	    PomFiles.showTime(driver).click();
	    PomFiles.acceptpopupButton(driver).click();
	    Thread.sleep(5000);
	    PomFiles.tickets(driver).click();
	    PomFiles.procceedQty(driver).click();
        Thread.sleep(5000);
	    List<WebElement> aseats;
	    List<WebElement> sseats;
	    int i=0;
	    int selected = 0;
	    List<WebElement> rows = driver.findElements(By.xpath("//td/div[@class='seatR Setrow1']"));
	    while(selected<Integer.parseInt(noOfTickets)){
	    WebElement row = rows.get(i);
	    aseats = row.findElements(By.xpath("../following-sibling::td//a[@class='_available']"));
	    while(selected<Integer.parseInt(noOfTickets)){
	    if(aseats.size()>0){
	    aseats.get(0).click();
	    sseats = row.findElements(By.xpath("../following-sibling::td//a[@class='_available _selected']"));
	    aseats = row.findElements(By.xpath("../following-sibling::td//a[@class='_available']"));
	    selected+=sseats.size();
	    System.out.println(rows.size());
	  }
	  }
	    i++;
	    if(i==rows.size())
	    break;
	  }
	    Thread.sleep(10000);
        PomFiles.btmcmntBook(driver).click();
        Thread.sleep(10000);
        PomFiles.shmTicket(driver).click();
	    PomFiles.prepay(driver).click();
	    Thread.sleep(3000);
       }
		
    @DataProvider(name="Login")
	public Object [][] data() {
		return new Object[][] {
			{"Enna solla pogirai","chennai"},
			{"kombu vatcha singamda","coimbatore"}
	};
	}
		
		@AfterClass
		public void tearDown() {
			driver.close();

    }
    }
